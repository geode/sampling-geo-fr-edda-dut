# Echantillonnage des articles de Géographie française dans l'Encyclopédie de Diderot et d'Alembert

Ce dépôt est proposé par **Ludovic Moncla** et **Denis Vigier** dans le cadre du [Projet GEODE](https://geode-project.github.io/).
Il contient le code développé pour la sélection d'un échantillon d'articles représentatifs traitant de géographie française dans l'Encyclopédie de Diderot et d'Alembert (EDdA) et le Dictionnaire Universel de Trevoux (DUT).

## Présentation

![](./figures/schema.png)



## Remerciements

Les auteurs remercient le [LABEX ASLAN](https://aslan.universite-lyon.fr/) (ANR-10-LABX-0081) de l'Université de Lyon pour son soutien financier dans le cadre du programme français  "Investissements d'Avenir" géré par l'Agence Nationale de la Recherche  (ANR).
